/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import MainApp from './sources';
import './sources/Language/config';

const App: () => React$Node = () => {
  return <MainApp />;
};

export default App;
