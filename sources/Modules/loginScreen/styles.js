/* eslint-disable prettier/prettier */
import {StyleSheet} from 'react-native';
import color from '../../Helpers/color';
export default StyleSheet.create({
  textButton: {
    fontSize: 15,
    fontWeight: '600',
    color: color.Neutral_100,
  },
  touchableOpacity: {
    width: 340,
    height: 48,
    borderRadius: 24,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    width: 343,
    height: 44,
    borderWidth: 1,
    borderColor: color.Neutral_300,
    backgroundColor: color.white,
    borderRadius: 4,
    paddingLeft: 48,
    marginTop: 12.5,
  },
});
