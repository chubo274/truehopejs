/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { Image, Linking, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ICON } from '../../assets';
import ButtonCustom from '../../Components/button';
import ButtonHotline from '../../Components/buttonHotline';
import ButtonLinear from '../../Components/buttonLinear';
import TextInputCustom from '../../Components/textInput';
import { constant } from '../../Constant/index';
import color from '../../Helpers/color';
import { login } from '../../Redux/Actions/userActions';
import ActionTypes from '../../Redux/ActionTypes';
import styles from './styles';
import { useTranslation } from 'react-i18next';

const LoginScreen = () => {
  const { t } = useTranslation();
  //! State
  const dataUser = useSelector((state) => state.userReducer.LoginReducer);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [eye, setEye] = useState(true);

  //! Fuction
  const callPhone = () => {
    const phoneNumber = `tel:${constant.SDT}`;
    Linking.openURL(phoneNumber);
  };
  const changeUseName = (params) => {
    setUserName(params);
  };
  const changePassword = (params) => {
    setPassword(params);
  };
  const changeEye = () => {
    setEye(!eye);
  };

  //! UseEffect
  useEffect(() => {
    if (dataUser?.type === ActionTypes.LOGIN_SUCCESS) {
      dispatch({ type: ActionTypes.REFRESH_TYPE });
    }
  }, [dataUser]);
  //! Render
  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <Image style={{ height: 55.76, marginTop: 66, marginHorizontal: 70 }} source={ICON.logoTrueHope} />
          <View style={{ marginTop: 78 }}>
            <ButtonLinear
              colors={[color.Gradient_2_1, color.Gradient_2_2]}
              angle={270}
              angleCenter={{ x: 0.5, y: 0.5 }}
              textButton={t('LoginScreen.loginWithFB')}
              urlLeft={ICON.iconFaceBook}
            />
            <ButtonCustom
              urlLeft={ICON.iconApple}
              textButton={t('LoginScreen.loginWithAplle')}
              style={{ backgroundColor: color.Neutral_800, marginTop: 12 }}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 18,
              paddingHorizontal: 16,
            }}>
            <View
              style={{
                flex: 1,
                borderWidth: 1,
                height: 0,
                borderColor: color.Neutral_700,
              }}
            />
            <Text style={{ marginHorizontal: 16 }}>{t('LoginScreen.or')}</Text>
            <View
              style={{
                flex: 1,
                borderWidth: 1,
                height: 0,
                borderColor: color.Neutral_700,
              }}
            />
          </View>

          <TextInputCustom
            nameIconLeft={'phone'}
            placeholder={t('LoginScreen.phone')}
            nameIconRight={'x'}
            keyboardType="numeric"
            value={userName}
            onChangeText={changeUseName}
            onPressRight={() => {
              changeUseName('');
            }}
          />
          <TextInputCustom
            nameIconLeft={'lock'}
            placeholder={t('LoginScreen.passWord')}
            value={password}
            onChangeText={changePassword}
            secureTextEntry={eye}
            onPressRight={() => {
              changeEye();
            }}
            nameIconRight={eye ? 'eye-off' : 'eye'}
          />

          <View
            style={{
              flexDirection: 'row',
              marginTop: 12.5,
              flex: 1,
              marginHorizontal: 16,
            }}>
            <TouchableOpacity
              onPress={() => {
                dispatch(login({ phone: userName, password }));
              }}
              style={[
                styles.touchableOpacity,
                {
                  width: 258,
                  height: 48,
                  backgroundColor: color.Primary_400,
                  borderTopRightRadius: 0,
                  borderBottomRightRadius: 0,
                  marginRight: 8,
                },
              ]}>
              <Text style={styles.textButton}>{t('LoginScreen.login')}</Text>
              <Image style={{ width: 20, height: 20 }} source={ICON.iconLogin} />
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.touchableOpacity,
                {
                  width: 77,
                  height: 48,
                  backgroundColor: color.Primary_400,
                  borderTopLeftRadius: 0,
                  borderBottomLeftRadius: 0,
                },
              ]}>
              <Image style={{ width: 24, height: 24 }} source={ICON.iconTouch_ID} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              flex: 1,
              marginHorizontal: 16,
              marginTop: 25,
              alignItems: 'center',
            }}>
            <TouchableOpacity style={{ marginBottom: 9 }}>
              <Text style={[styles.textButton, { color: color.Accent_900 }]}>{t('LoginScreen.forgotPassword')}</Text>
            </TouchableOpacity>
            <View style={{ flexDirection: 'row' }}>
              <Text style={[styles.textButton, { color: color.Neutral_700 }]}>{t('LoginScreen.noAccount')}</Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('StackRegister');
                }}>
                <Text style={[styles.textButton, { color: color.Accent_900 }]}>{t('LoginScreen.register')}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text
            style={[
              styles.textButton,
              {
                flex: 1,
                marginHorizontal: 16,
                color: color.Neutral_600,
                marginTop: 95,
                marginBottom: 20,
                textAlign: 'center',
              },
            ]}>
            {t('LoginScreen.iHaveHard')}
          </Text>
          <View style={{ marginBottom: 44 }}>
            <ButtonHotline
              urlLeft={ICON.iconPhoneHotline}
              text={`Hotline ${constant.SDT}`}
              onPress={() => {
                callPhone();
              }}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default LoginScreen;
