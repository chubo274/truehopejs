/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, View } from 'react-native';
import Header from '../../Components/Header';

const ThongBaoScreen = () => {
  const { t } = useTranslation();
  return (
    <View>
      <Header
        textHeader={t('ThongBaoScreen.notification')}
        iconLeft={'chevron-left'}
        iconRight1={'search'}
        iconRight2={'filter'}
      />
    </View>
  );
};

export default ThongBaoScreen;

const styles = StyleSheet.create({});
