/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import color from '../../Helpers/color';

const SearchScreen = () => {
  //! State
  const [search, setSearch] = useState();
  //! Function
  const onChangeSearch = (params) => {
    setSearch(params);
  };

  //! UseEffect
  //! Render
  return (
    <View>
      <View
        style={{
          backgroundColor: color.new_bg_900,
          height: 56,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
        }}>
        <Feather name="chevron-left" size={20} color={color.white} style={{ marginHorizontal: 16 }} />
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: 'center',
            borderBottomWidth: 1,
            borderBottomColor: color.white,
          }}>
          <Feather name="search" size={20} color={color.white} style={{ marginHorizontal: 16 }} />
          <TextInput
            style={{ flex: 1, color: color.white }}
            onChangeText={onChangeSearch}
            value={search}
            placeholder="Search"
            placeholderTextColor={color.white}
          />
        </View>
        <Feather name="filter" size={20} color={color.white} style={{ marginHorizontal: 16 }} />
      </View>
      <View />
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({});
