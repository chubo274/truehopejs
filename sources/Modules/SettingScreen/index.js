/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { StyleSheet, Switch, Text, TouchableOpacity, View } from 'react-native';
import color from '../../Helpers/color';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { logout } from '../../Redux/Actions/userActions';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

const SettingScreen = () => {
  const { t } = useTranslation();
  //! State
  const dispatch = useDispatch();
  const [isEnabled, setIsEnabled] = useState(false);
  const [languageVIE, setLanguageVIE] = useState(true);
  const [languageENG, setLanguageENG] = useState(false);
  //! Funtion
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  const changeLanguageVIE = (params) => {
    setLanguageVIE(true);
    setLanguageENG(false);
  };
  const changeLanguageENG = (params) => {
    setLanguageVIE(false);
    setLanguageENG(true);
  };

  //! UseEffect
  //! Render
  return (
    <View>
      <View style={{ backgroundColor: color.new_bg_900, height: 56, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ color: color.white, fontSize: 18, fontWeight: 'bold' }}>
          {t('SettingScreen.privacySettings')}
        </Text>
      </View>
      <View style={{ marginHorizontal: 16, marginTop: 23, backgroundColor: color.white }}>
        <View style={{ flexDirection: 'row', paddingVertical: 14 }}>
          <Feather name="map" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
          <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
            {t('SettingScreen.language')}
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={changeLanguageVIE}>
              <View style={{ marginHorizontal: 10, flexDirection: 'row', alignItems: 'center' }}>
                {languageVIE ? (
                  <View
                    style={{
                      width: 24,
                      height: 24,
                      borderWidth: 1.5,
                      borderColor: color.Accent_700,
                      borderRadius: 12,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{ width: 14, height: 14, borderRadius: 7, backgroundColor: color.Accent_700 }} />
                  </View>
                ) : (
                  <View
                    style={{
                      width: 24,
                      height: 24,
                      borderWidth: 1.5,
                      borderColor: color.Neutral_400,
                      borderRadius: 12,
                    }}
                  />
                )}
                <Text> VIE</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={changeLanguageENG}>
              <View style={{ marginHorizontal: 10, flexDirection: 'row', alignItems: 'center' }}>
                {languageENG ? (
                  <View
                    style={{
                      width: 24,
                      height: 24,
                      borderWidth: 1.5,
                      borderColor: color.Accent_700,
                      borderRadius: 12,
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <View style={{ width: 14, height: 14, borderRadius: 7, backgroundColor: color.Accent_700 }} />
                  </View>
                ) : (
                  <View
                    style={{
                      width: 24,
                      height: 24,
                      borderWidth: 1.5,
                      borderColor: color.Neutral_400,
                      borderRadius: 12,
                    }}
                  />
                )}
                <Text> ENG</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
          <Feather name="unlock" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
          <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
            {t('SettingScreen.changePassword')}
          </Text>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', paddingVertical: 14 }}>
          <MaterialIcons name="fingerprint" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
          <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
            {t('SettingScreen.fingerprintSettings')}
          </Text>
          <TouchableOpacity>
            <Switch
              trackColor={{ false: color.Neutral_500, true: color.Accent_500 }}
              thumbColor={color.Neutral_100}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{ flexDirection: 'row', paddingVertical: 14 }}
          onPress={() => {
            dispatch(logout());
          }}>
          <Feather name="log-out" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
          <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
            {t('SettingScreen.logOut')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SettingScreen;

const styles = StyleSheet.create({});
