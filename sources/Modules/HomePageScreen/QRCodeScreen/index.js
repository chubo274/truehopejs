/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import color from '../../../Helpers/color';
import ButtonCustom from '../../../Components/button';
import { useDispatch, useSelector } from 'react-redux';
import { logout, QRCode } from '../../../Redux/Actions/userActions';
import { useTranslation } from 'react-i18next';
import Header from '../../../Components/Header';
import { useNavigation } from '@react-navigation/native';

const QRCodeScreen = () => {
  const { t } = useTranslation();
  //! State
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const dataReducer = useSelector((state) => state.userReducer);
  console.log({ dataReducer });
  const [failed, setFailed] = useState();
  const [success, setSuccess] = useState();
  console.log({ success });
  //! Funtion
  const onChangeSuccess = (params) => {
    setSuccess(params);
  };
  const onChangeFailed = (params) => {
    setFailed(params);
  };

  //! UseEffect
  useEffect(() => {
    dispatch(
      QRCode({
        onSuccess: (value) => {
          onChangeSuccess(value);
        },
        onFailed: (value) => {
          onChangeFailed(value);
        },
      }),
    );
  }, []);
  //! Render
  return (
    <View>
      <Header
        textHeader={t('QRCodeScreen.QRCode')}
        iconLeft={'chevron-left'}
        onPressIconLeft={() => {
          navigation.goBack();
        }}
      />
      <View
        style={{
          marginTop: 25,
          marginHorizontal: 16,
          backgroundColor: color.white,
          alignItems: 'center',
          borderRadius: 8,
        }}>
        <Text style={{ fontSize: 18, fontWeight: '700', marginVertical: 28, color: color.Neutral_900 }}>
          Hoàng Anh Tuấn
        </Text>
        <View style={{ width: 128, height: 128, backgroundColor: 'red' }} />
        <Text style={{ marginHorizontal: 52, marginVertical: 28, fontSize: 13, textAlign: 'center' }}>
          {t('QRCodeScreen.scanCodeAtTrueHope')}
        </Text>
        <View style={{ marginTop: 49, marginVertical: 26, height: 48 }}>
          <ButtonCustom
            textButton={t('QRCodeScreen.goBackMainPage')}
            style={{ backgroundColor: 'white', borderWidth: 1, borderColor: color.Primary_500 }}
            styleText={{ color: color.Primary_500 }}
          />
        </View>
      </View>
    </View>
  );
};

export default QRCodeScreen;

const styles = StyleSheet.create({});
