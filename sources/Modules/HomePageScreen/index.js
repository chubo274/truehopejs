/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import { img } from '../../assets/imgHomeScreen';
import ButtonCustom from '../../Components/button';
import color from '../../Helpers/color';
import { getBookings, getNews, getOffers } from '../../Redux/Actions/homeActions';

const width = Dimensions.get('window').width;
const HomePage = () => {
  const { t } = useTranslation();
  //! UseState
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [offset, setOffset] = useState();
  const [limit, setLimit] = useState();
  const [valueBookings, setValueBookings] = useState();
  const [valueOffers, setValueOffers] = useState();
  const dataReducer = useSelector((state) => state.userReducer);
  const dataTinTucReducer = useSelector((state) => state.homeReducer.tinTucReducer.data);
  const { count, results } = dataTinTucReducer;
  //! Function
  const changeValueBookings = (params) => {
    setValueBookings(params);
  };
  const changeValueOffers = (params) => {
    setValueOffers(params);
  };

  //! UseEffect
  useEffect(() => {
    dispatch(getNews({ limit, offset }));
    dispatch(
      getBookings({
        onSuccess: (value) => {
          changeValueBookings(value);
        },
        onFailed: (error) => {
          Alert.alert(
            t('HomeScreen.AnErrorOccurred'),
            error,
            [
              {
                text: 'OkOkOk',
              },
            ],
            { cancelable: false },
          );
        },
      }),
    );
    dispatch(
      getOffers({
        onSuccess: (value) => {
          changeValueOffers(value);
        },
        onFailed: (error) => {
          Alert.alert(
            t('HomeScreen.AnErrorOccurred'),
            error,
            [
              {
                text: 'Ok',
              },
            ],
            { cancelable: false },
          );
        },
      }),
    );
  }, []);
  //! Render
  return (
    // <SafeAreaView>
    <View style={{ flex: 1, backgroundColor: color.new_bg_900 }}>
      <ScrollView>
        <ImageBackground source={require('../../assets/img/Vector.png')} style={{ width: '100%' }}>
          <View
            style={{
              flexDirection: 'row',
              height: 60,
              marginTop: 10,
              marginHorizontal: 24,
              marginBottom: 20,
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                backgroundColor: 'blue',
                width: 50,
                height: 50,
                marginRight: 16,
                borderRadius: 24,
              }}>
              {/* <Image source={dataReducer.LoginReducer?.data?.user?.}/> */}
            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>
                {`${t('HomeScreen.hello')} ${dataReducer.LoginReducer?.data?.user?.last_name} ${
                  dataReducer.LoginReducer?.data?.user?.first_name
                } !`}
              </Text>
              <View style={{ flexDirection: 'row' }}>
                <Image source={img.point} />
                <Text style={{ color: 'white' }}> {t('HomeScreen.TruePoint')}</Text>
              </View>
            </View>
            <View>
              <TouchableOpacity
                style={{ width: 45, height: 45 }}
                onPress={() => {
                  navigation.navigate('QRCodeScreen');
                }}>
                <Image source={img.QRCode} />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              marginHorizontal: 16,
              backgroundColor: color.white,
              borderRadius: 8,
              padding: 0,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderRadius: 8,
                width: '100%',
                height: 80,
                backgroundColor: color.white,
              }}>
              <View style={{ height: 78, justifyContent: 'center', marginHorizontal: 14 }}>
                <Image source={img.iconHome} style={{ width: 48, height: 48 }} />
              </View>
              <View style={{ justifyContent: 'center', width: 251, height: 32 }}>
                <Text>
                  {t('HomeScreen.notForgetYourAppointment')}
                  <Text style={{ fontWeight: 'bold' }}>{`15:00 ${t('HomeScreen.day')} 12/11`}</Text>
                </Text>
              </View>
            </View>
            <View
              style={{
                backgroundColor: 'white',
                opacity: 0.3,
                width: '100%',
                height: 1,
                shadowColor: color.Shadow,
                elevation: 4,
              }}
            />
            <View style={{ alignItems: 'center', height: 40, justifyContent: 'center' }}>
              <TouchableOpacity>
                <Text style={{ color: color.Accent_900, fontWeight: 'bold', fontSize: 12 }}>
                  {t('HomeScreen.reviewAppointments')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
        {/* Quick Action */}
        <View
          style={{ marginTop: 25, backgroundColor: 'white', flex: 1, borderTopRightRadius: 8, borderTopLeftRadius: 8 }}>
          <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 16 }}>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgDatKham} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.bookAnAppointment')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgTVTT} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.onlineCounseling')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgNhaThuoc} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.onlinePharmacy')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgTinTucSK} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.healthNews')}</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', margin: 16 }}>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgTheoDoiCHiSo} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.indexTracking')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgYBaDienTu} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.electronicMedicalRecords')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgNhacNhoSucKhoe} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.healthReminder')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ width: 60 }}>
                <Image source={img.ImgKhanCap} style={{ width: 60, height: 60 }} />
                <Text style={styles.TextQuickAction}>{t('HomeScreen.emergency')}</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                backgroundColor: 'white',
                opacity: 0.3,
                width: '100%',
                height: 1,
                elevation: 4,
              }}
            />
          </View>
          <View style={{ flex: 1, marginHorizontal: 16, marginVertical: 24, height: 100 }}>
            <FlatList
              horizontal
              pagingEnabled
              showsHorizontalScrollIndication={false}
              data={valueBookings}
              renderItem={({ item, index }) => (
                <View style={{ backgroundColor: color.Neutral_300, borderRadius: 12 }}>
                  <Image
                    source={{ uri: `${item.package.image}` }}
                    style={{
                      width: width - 32,
                      height: 100,
                      borderRadius: 12,
                    }}
                    resizeMode={'center'}
                  />
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: color.Accent_900, marginHorizontal: 16 }}>
              {t('HomeScreen.otherOffers')}
            </Text>
            <View style={{ marginHorizontal: 14, marginVertical: 8, flex: 1, flexDirection: 'row' }}>
              <FlatList
                numColumns={2}
                data={valueOffers}
                renderItem={({ item, index }) => (
                  <View style={{ width: (width - (16 + 44)) / 2, marginHorizontal: 8 }}>
                    <Image
                      source={{ uri: `${item.image}` }}
                      style={{
                        width: '100%',
                        height: (width - (16 + 44)) / 2,
                        backgroundColor: color.Neutral_300,
                        borderRadius: 8,
                        marginVertical: 4,
                      }}
                      resizeMode={'cover'}
                    />
                    <Text style={styles.TextUuDai}>{item.title}</Text>
                    <Text style={styles.TextUuDaiSecond}>
                      {`${t('HomeScreen.to')} ${moment(item.expired_date).format('DD-MM')}`}
                    </Text>
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>
          <View>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: color.Accent_900, marginHorizontal: 16 }}>
              {t('HomeScreen.healthNews')}
            </Text>
            <FlatList
              data={results}
              renderItem={({ item, index }) => (
                <View style={{ flex: 1, flexDirection: 'row', margin: 16 }}>
                  <View
                    style={{
                      width: 108,
                      height: 92,
                      backgroundColor: color.Neutral_300,
                      borderRadius: 8,
                      marginRight: 13,
                    }}>
                    <Image
                      source={{ uri: `${item.image}` }}
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: 8,
                        marginRight: 13,
                      }}
                    />
                  </View>
                  <View style={{ flex: 1, justifyContent: 'space-between' }}>
                    <Text style={{ fontSize: 12, color: color.Accent_700 }}>{item.categories[0]?.name}</Text>
                    <Text style={{ fontSize: 12, color: color.Neutral_900, fontWeight: '700' }}>{item?.title}</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <Text style={{ fontSize: 12, color: color.Neutral_500 }}>
                        {`${moment(item.created).format('hh')} ${t('HomeScreen.hoursAgo')}`}
                      </Text>
                      <View style={{ flexDirection: 'row' }}>
                        <Feather name="eye" size={16} color={color.Neutral_500} />
                        <Text style={{ fontSize: 13, color: color.Neutral_500 }}> {item.count}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          <View style={{ marginTop: 16, marginBottom: 60 }}>
            <ButtonCustom
              textButton={t('HomeScreen.readMoreNews')}
              style={{ backgroundColor: 'white', borderWidth: 1, borderColor: color.Primary_500, height: 45 }}
              styleText={{ color: color.Primary_500 }}
              onPress={() => {}}
            />
          </View>
        </View>
      </ScrollView>
    </View>
    // </SafeAreaView>
  );
};

export default HomePage;

const styles = StyleSheet.create({
  TextQuickAction: { fontSize: 12, textAlign: 'center' },
  TextUuDai: { fontSize: 13, fontWeight: 'bold', color: color.black },
  TextUuDaiSecond: { fontSize: 12, color: color.Neutral_700, marginVertical: 4 },
});
