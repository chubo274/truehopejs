/* eslint-disable react-hooks/rules-of-hooks */
import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ActionTypes from '../Redux/ActionTypes';

const test = () => {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.userReducer);
  console.log({ data });
  return (
    <View>
      <Text>123</Text>
      <Text>123</Text>
      <Text>123</Text>
      <Text>123</Text>
      <Button
        title="Đăng Xuất"
        onPress={() => {
          dispatch({ type: ActionTypes.LOG_OUT });
        }}
      />
    </View>
  );
};

export default test;

const styles = StyleSheet.create({});
