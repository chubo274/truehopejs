/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { ICON } from '../../../assets';
import CheckCustom from '../../../Components/check';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { useNavigation } from '@react-navigation/native';
import { useRoute } from '@react-navigation/core';
import { useDispatch, useSelector } from 'react-redux';
import ActionTypes from '../../../Redux/ActionTypes';
import { login, register } from '../../../Redux/Actions/userActions';
import color from '../../../Helpers/color';
import { useTranslation } from 'react-i18next';

const RegisterOtpScreen = () => {
  const { t } = useTranslation();
  //! State
  const typeRegister = useSelector((state) => state.userReducer.RegisterReducer.type);
  const userReducer = useSelector((state) => state.userReducer);
  console.log('userReducer', userReducer);
  console.log({ typeRegister });
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route = useRoute();
  const { dataRegister } = route.params;
  console.log({ dataRegister });
  const [code, setCode] = useState();
  //! Function
  const changeCode = (params) => {
    setCode(params);
  };
  //! UseEffect
  useEffect(() => {
    if (typeRegister === ActionTypes.REGISTER_SUCCESS) {
      dispatch(login(dataRegister));
      dispatch({ type: ActionTypes.REFRESH_TYPE });
      navigation.navigate('RegisterInfoScreen');
    }
  }, [typeRegister]);
  //! render
  return (
    <SafeAreaView>
      <View>
        <ScrollView>
          <CheckCustom page={2} />
          <Text
            style={{
              height: 32,
              fontSize: 26,
              fontWeight: 'bold',
              color: color.Accent_900,
              textAlign: 'center',
              marginTop: 29,
            }}>
            {t('RegisterOtpScreen.importOTP')}
          </Text>
          <Image style={{ marginHorizontal: 97, marginTop: 9 }} source={ICON.logoOTP} />
          <Text
            style={{
              marginHorizontal: 47,
              fontSize: 13,
              color: color.Neutral_700,
              marginTop: 14,
            }}>
            {t('RegisterOtpScreen.OTPSentToPhone')}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 17,
              justifyContent: 'center',
            }}>
            <SmoothPinCodeInput
              value={code}
              onTextChange={changeCode}
              limitToNumbers
              autoFocus
              cellSpacing={4}
              cellStyle={{
                height: 40,
                width: 40,
                borderWidth: 1,
                borderRadius: 20,
                borderColor: color.Primary_200,
              }}
              cellStyleFocused={{
                borderWidth: 2,
                borderColor: color.Primary_200,
              }}
              onFulfill={() => {
                dispatch(register(dataRegister));
              }}
            />
          </View>
          <View style={{ flexDirection: 'row', marginHorizontal: 87, marginTop: 21 }}>
            <Text style={{ fontSize: 12, color: color.Neutral_700 }}>{t('RegisterOtpScreen.OTPCodeNotReceived')}</Text>
            <TouchableOpacity>
              <Text style={{ fontSize: 12, color: color.Accent_900 }}>{t('RegisterOtpScreen.sendTo')}</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default RegisterOtpScreen;

const styles = StyleSheet.create({});
