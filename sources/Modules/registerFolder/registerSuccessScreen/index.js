/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, View, ScrollView, SafeAreaView, Image, Linking } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ICON } from '../../../assets';
import ButtonHotline from '../../../Components/buttonHotline';
import ButtonCustom from '../../../Components/button';
import { constant } from '../../../Constant/index';
import { useSelector } from 'react-redux';
import color from '../../../Helpers/color';
import { useTranslation } from 'react-i18next';

const RegisterSuccessScreen = () => {
  const { t } = useTranslation();
  const dataLogin = useSelector((state) => state.userReducer.LoginReducer);
  console.log({ dataLogin });
  const navigation = useNavigation();

  const callPhone = () => {
    const phoneNumber = `tel:${constant.SDT}`;
    Linking.openURL(phoneNumber);
  };
  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <Image source={ICON.iconRegisterSuccess} />
          <Text
            style={{
              marginHorizontal: 76,
              marginTop: 59,
              fontSize: 22,
              color: color.Accent_900,
              fontWeight: 'bold',
            }}>
            {t('RegisterSuccessScreen.youAreConnected')}
          </Text>
          <Text
            style={{
              marginHorizontal: 24,
              fontSize: 15,
              color: color.Neutral_700,
              marginTop: 24,
            }}>
            {t('RegisterSuccessScreen.yourAccountHasBeenCreated')}
          </Text>
          <View style={{ marginTop: 241, marginBottom: 16 }}>
            <ButtonCustom
              textButton={t('RegisterSuccessScreen.goBackMainPage')}
              style={{ backgroundColor: 'white', borderWidth: 1, borderColor: color.Primary_500 }}
              styleText={{
                color: color.Primary_500,
              }}
              onPress={() => {
                navigation.navigate('HomePage');
              }}
            />
          </View>
          <View style={{ marginBottom: 44 }}>
            <ButtonHotline
              urlLeft={ICON.iconPhoneHotline}
              text={`Hotline ${constant.SDT}`}
              onPress={() => {
                callPhone();
              }}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default RegisterSuccessScreen;

const styles = StyleSheet.create({});
