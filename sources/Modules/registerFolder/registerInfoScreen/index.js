/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { ICON } from '../../../assets';
import ButtonLinear from '../../../Components/buttonLinear';
import CheckCustom from '../../../Components/check';
import DateTime from '../../../Components/datetime';
import TextInputCustom from '../../../Components/textInput';
import color from '../../../Helpers/color';
import { updateUserInfo } from '../../../Redux/Actions/userActions';
import ActionTypes from '../../../Redux/ActionTypes';

const RegisterInfoScreen = () => {
  const { t } = useTranslation();
  const typeRegisterInfo = useSelector((state) => state.userReducer.UpdateUserInfoReducer?.type);
  console.log({ typeRegisterInfo });
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [isMale, setIsMale] = useState(false);
  const [isFemale, setIsFemale] = useState(false);

  const validationSchema = Yup.object().shape({
    name: Yup.string().required(`${t('Formik.nameRequied')}`),
    dob: Yup.string().required(`${t('Formik.dobRequied')}`),
    email: Yup.string()
      .required(`${t('Formik.emailRequied')}`)
      .matches(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i, `${t('Formik.emailInvalid')}`),
    gender: Yup.string().required(`${t('Formik.genderRequied')}`),
  });
  const initialErrors = {
    name: `${t('Formik.nameRequied')}`,
    dob: `${t('Formik.dobRequied')}`,
    email: `${t('Formik.emailRequied')}`,
    gender: `${t('Formik.genderRequied')}`,
  };
  const onSubmit = (data) => {
    console.log(data);
    console.log(typeRegisterInfo);
    dispatch(updateUserInfo({ ...data, dob: moment(data.dob).format('YYYY-MM-DD') }));
  };
  const showErrors = (error) => {
    if (!isEmpty(error)) {
      Alert.alert(error.name || error.dob || error.email);
    }
    return;
  };
  useEffect(() => {
    if (typeRegisterInfo === ActionTypes.UPDATE_USER_INFO_SUCCESS) {
      dispatch({ type: ActionTypes.REFRESH_TYPE });
      navigation.navigate('RegisterSuccessScreen');
    }
  }, [typeRegisterInfo]);
  return (
    <SafeAreaView>
      <ScrollView>
        <Formik
          initialValues={{
            name: '',
            dob: moment().toDate(),
            email: '',
            gender: '',
          }}
          initialErrors={initialErrors}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
          validateOnBlur={false}>
          {({ handleChange, handleSubmit, errors, values, setFieldValue }) => {
            const onCheckGender = (Male, Female) => {
              if (Female) {
                if (Male === false) {
                  setFieldValue('gender', 'f');
                }
              } else {
                if (Male) {
                  setFieldValue('gender', 'm');
                } else {
                  setFieldValue('gender', 'u');
                }
              }
            };

            const onChanegeIsMale = () => {
              setIsMale(!isMale);
              setIsFemale(false);
              onCheckGender(!isMale, false);
            };

            const onChanegeIsFemale = () => {
              setIsMale(false);
              setIsFemale(!isFemale);
              onCheckGender(false, !isFemale);
            };

            return (
              <View>
                <CheckCustom page={3} />
                <Text
                  style={{
                    height: 32,
                    fontSize: 26,
                    fontWeight: 'bold',
                    color: color.Accent_900,
                    textAlign: 'center',
                    marginTop: 29,
                  }}>
                  {t('RegisterInfoScreen.yourInformation')}
                </Text>
                <View
                  style={{
                    marginHorizontal: 60,
                    marginTop: 8,
                    marginBottom: 12,
                  }}>
                  <Text style={[styles.text, { color: color.Neutral_700 }]}>
                    {t('RegisterInfoScreen.completePersonalInformation')}
                  </Text>
                </View>
                <View>
                  <TextInputCustom
                    nameIconLeft={'phone'}
                    placeholder={t('RegisterInfoScreen.name')}
                    value={values.name}
                    onChangeText={handleChange('name')}
                  />
                  <DateTime
                    disabled={false}
                    datetime={values.dob}
                    changeDatetime={(date) => setFieldValue('dob', date)}
                  />
                  <TextInputCustom
                    nameIconLeft={'mail'}
                    placeholder="Email"
                    value={values.email}
                    onChangeText={handleChange('email')}
                  />
                </View>
                <View style={{ marginTop: 36 }}>
                  <Text style={styles.text}>{t('RegisterInfoScreen.gender')}</Text>
                </View>
                <View
                  style={{
                    marginHorizontal: 48,
                    height: 152,
                    flexDirection: 'row',
                  }}>
                  <TouchableOpacity
                    style={{ flex: 1, marginTop: 5 }}
                    onPress={() => {
                      onChanegeIsMale();
                    }}>
                    {isMale ? (
                      <View style={{ opacity: 1, alignItems: 'center' }}>
                        <Image source={ICON.iconNam} />
                        <Text style={{ fontSize: 15, color: color.Neutral_700, marginTop: 19 }}>
                          {t('RegisterInfoScreen.male')}
                        </Text>
                      </View>
                    ) : (
                      <View style={{ opacity: 0.5, alignItems: 'center' }}>
                        <Image source={ICON.iconNam} />
                        <Text style={{ fontSize: 15, color: color.Neutral_700, marginTop: 19 }}>
                          {t('RegisterInfoScreen.male')}
                        </Text>
                      </View>
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => {
                      onChanegeIsFemale();
                    }}>
                    {isFemale ? (
                      <View style={{ opacity: 1, alignItems: 'center' }}>
                        <Image source={ICON.iconNu} />
                        <Text style={{ fontSize: 15, color: color.Neutral_700, marginTop: 8 }}>
                          {t('RegisterInfoScreen.female')}
                        </Text>
                      </View>
                    ) : (
                      <View style={{ opacity: 0.5, alignItems: 'center' }}>
                        <Image source={ICON.iconNu} />
                        <Text style={{ fontSize: 15, color: color.Neutral_700, marginTop: 8 }}>
                          {t('RegisterInfoScreen.female')}
                        </Text>
                      </View>
                    )}
                  </TouchableOpacity>
                </View>

                <View style={{ marginVertical: 64 }}>
                  <ButtonLinear
                    colors={[color.gradient_button_1, color.gradient_button_2]}
                    angleCenter={{ x: 0, y: 0.5 }}
                    textButton={t('RegisterInfoScreen.createAccount')}
                    onPress={() => {
                      showErrors(errors);
                      handleSubmit();
                    }}
                  />
                </View>
              </View>
            );
          }}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};
export default RegisterInfoScreen;

const styles = StyleSheet.create({
  text: { fontSize: 13, color: color.Neutral_700, textAlign: 'center' },
});
