/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { Formik } from 'formik';
import * as Yup from 'yup';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, Linking, ScrollView, Alert } from 'react-native';
import { ICON } from '../../../assets';
import TextInputCustom from '../../../Components/textInput';
import ButtonLinear from '../../../Components/buttonLinear';
import ButtonHotline from '../../../Components/buttonHotline';
import { constant } from '../../../Constant/index';
import CheckCustom from '../../../Components/check';
import { useNavigation } from '@react-navigation/native';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { checkPhoneRegister } from '../../../Redux/Actions/userActions';
import ActionTypes from '../../../Redux/ActionTypes';
import color from '../../../Helpers/color';
import { useTranslation } from 'react-i18next';

const RegisterScreen = () => {
  const { t } = useTranslation();
  //! State
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const typeCheckPhone = useSelector((state) => state.userReducer.CheckPhoneReducer.type);
  console.log({ typeCheckPhone });
  const [eyePassword, setEyePassword] = useState(true);
  const [eyeRePassword, setEyeRePassword] = useState(true);
  const [dataUser, setDataUser] = useState({});
  //! Function
  const callPhone = () => {
    const phoneNumber = `tel:${constant.SDT}`;
    Linking.openURL(phoneNumber);
  };
  const changeEyePassword = () => {
    setEyePassword(!eyePassword);
  };
  const changeEyeRePassword = () => {
    setEyeRePassword(!eyeRePassword);
  };
  const changeDataUser = (value) => {
    setDataUser(value);
  };

  const validationSchema = Yup.object().shape({
    phone: Yup.string()
      .required(`${t('Formik.phoneRequied')}`)
      .matches(/^[0-9]{9,12}$/, `${t('Formik.phoneInvalid')}`),
    password: Yup.string()
      .required(`${t('Formik.passwordRequied')}`)
      .min(8, 'Password Ít nhất 8 ký tự'),
    rePassword: Yup.string()
      .required(`${t('Formik.passwordRequied')}`)
      .oneOf([Yup.ref('password'), null], `${t('Formik.passwordNotMatching')}`),
  });
  const initialErrors = {
    phone: `${t('Formik.phoneRequied')}`,
    password: `${t('Formik.passwordRequied')}`,
  };
  const onSubmit = ({ phone, password }) => {
    dispatch(checkPhoneRegister({ phone }));
    changeDataUser({ phone, password });
  };
  const showErrors = (error) => {
    if (!isEmpty(error)) {
      Alert.alert(error.phone || error.password || error.rePassword);
    }
    return;
  };
  //! UseEffect
  useEffect(() => {
    if (typeCheckPhone === ActionTypes.CHECK_PHONE_SUCCESS) {
      dispatch({ type: ActionTypes.REFRESH_TYPE });
      navigation.navigate('RegisterOtpScreen', {
        dataRegister: dataUser,
      });
    }
  }, [typeCheckPhone]);
  //! Render
  return (
    <SafeAreaView>
      <ScrollView>
        <Formik
          initialValues={{
            phone: '',
            password: '',
            rePassword: '',
          }}
          initialErrors={initialErrors}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
          validateOnBlur={false}>
          {({ handleChange, handleSubmit, errors, values, setFieldValue }) => {
            return (
              <View>
                <CheckCustom page={1} />

                <Text
                  style={{
                    height: 32,
                    marginVertical: 29,
                    fontFamily: 'Inter',
                    fontWeight: 'bold',
                    fontSize: 26,
                    textAlign: 'center',
                    color: color.Accent_900,
                  }}>
                  {t('RegisterScreen.registerAccount')}
                </Text>
                <TextInputCustom
                  nameIconLeft={'phone'}
                  placeholder={t('RegisterScreen.phone')}
                  nameIconRight={'x'}
                  value={values.phone}
                  onChangeText={handleChange('phone')}
                  onPressRight={() => {
                    // changeUseName('');
                  }}
                  keyboardType="numeric"
                />
                <TextInputCustom
                  nameIconLeft={'lock'}
                  placeholder={t('RegisterScreen.passWord')}
                  value={values.password}
                  onChangeText={handleChange('password')}
                  secureTextEntry={eyePassword}
                  onPressRight={() => {
                    changeEyePassword();
                  }}
                  nameIconRight={eyePassword ? 'eye-off' : 'eye'}
                />
                <TextInputCustom
                  nameIconLeft={'lock'}
                  placeholder={t('RegisterScreen.rePassWord')}
                  value={values.rePassword}
                  onChangeText={handleChange('rePassword')}
                  secureTextEntry={eyeRePassword}
                  onPressRight={() => {
                    changeEyeRePassword();
                  }}
                  nameIconRight={eyeRePassword ? 'eye-off' : 'eye'}
                />

                <View style={{ marginTop: 16 }} />
                <ButtonLinear
                  colors={[color.gradient_button_1, color.gradient_button_2]}
                  angleCenter={{ x: 0, y: 0.5 }}
                  textButton={t('RegisterScreen.register')}
                  urlRight={ICON.iconDky}
                  onPress={() => {
                    showErrors(errors);
                    handleSubmit();
                  }}
                />
                <View
                  style={{
                    marginHorizontal: 16,
                    alignItems: 'center',
                    marginTop: 20,
                  }}>
                  <Text style={styles.text}>{t('RegisterScreen.byClickRegister')}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity>
                      <Text style={[styles.text, { color: color.Accent_900 }]}>{t('RegisterScreen.usageRules')}</Text>
                    </TouchableOpacity>
                    <Text style={styles.text}>{t('RegisterScreen.and')}</Text>
                    <TouchableOpacity>
                      <Text style={[styles.text, { color: color.Accent_900 }]}>
                        {t('RegisterScreen.informationSecurity')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style>
                  <Text
                    style={[
                      styles.textButton,
                      {
                        flex: 1,
                        marginHorizontal: 16,
                        color: color.Neutral_600,
                        marginTop: 95,
                        marginBottom: 20,
                        textAlign: 'center',
                      },
                    ]}>
                    {t('RegisterScreen.iHaveHard')}
                  </Text>
                  <View style={{ marginBottom: 44 }}>
                    <ButtonHotline
                      urlLeft={ICON.iconPhoneHotline}
                      text={`Hotline ${constant.SDT}`}
                      onPress={() => {
                        callPhone();
                      }}
                    />
                  </View>
                </View>
              </View>
            );
          }}
        </Formik>
      </ScrollView>
    </SafeAreaView>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  textInput: {
    width: 343,
    height: 44,
    borderWidth: 1,
    borderColor: color.Neutral_300,
    backgroundColor: color.white,
    borderRadius: 4,
    paddingLeft: 48,
    marginTop: 4,
    marginBottom: 12,
  },
  text: {
    fontSize: 12,
    fontFamily: 'Inter',
    fontWeight: '400',
    color: color.Neutral_700,
  },
});
