/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Text, View } from 'react-native';
import color from '../../../Helpers/color';

const LichHenSapDienRaScreen = () => {
  const { t } = useTranslation();
  return (
    <View>
      <View style={{ margin: 16, backgroundColor: color.white, borderRadius: 8 }}>
        <Text>Lịch hẹn sắp diễn ra</Text>
      </View>
    </View>
  );
};

export default LichHenSapDienRaScreen;

const styles = StyleSheet.create({});
