/* eslint-disable react-native/no-inline-styles */
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';
import { img } from '../../assets/imgHomeScreen';
import color from '../../Helpers/color';
import Feather from 'react-native-vector-icons/Feather';
import { ICON } from '../../assets';
import ButtonLinear from '../../Components/buttonLinear';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const CaNhanScreen = () => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dataReducer = useSelector((state) => state.userReducer);
  console.log({ dataReducer });
  return (
    <View style={{ flex: 1, backgroundColor: color.new_bg_900, height: 225 }}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'row',
            height: 52,
            marginTop: 10,
            marginHorizontal: 24,
            marginBottom: 20,
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              backgroundColor: 'blue',
              width: 48,
              height: 48,
              marginRight: 13,
              borderRadius: 24,
            }}
          />
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 12, fontWeight: '700', color: 'white' }}>
              {`${t('CaNhanScreen.hello')} ${dataReducer.LoginReducer?.data?.getDataUser?.data?.name}!`}
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Image source={img.point} />
              <Text style={{ color: 'white' }}> {t('CaNhanScreen.TruePoint')}</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              style={{ width: 40, height: 40 }}
              onPress={() => {
                navigation.navigate('QRCodeScreen');
              }}>
              <Image source={img.QRCode} />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            marginHorizontal: 16,
            backgroundColor: color.white,
            borderRadius: 8,
          }}>
          <Image
            source={ICON.background}
            style={{ width: '100%', height: 185, borderTopLeftRadius: 4, borderTopRightRadius: 4 }}
          />
          <View style={{ width: 160, position: 'absolute', top: 24, left: 25 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', color: color.Accent_800 }}>
              {t('CaNhanScreen.electronicMedicalRecords')}
            </Text>
            <Text style={{ fontSize: 12, marginTop: 12 }}>{t('CaNhanScreen.healthRecords')}</Text>
            <ButtonLinear
              colors={[color.gradient_button_1, color.gradient_button_2]}
              angleCenter={{ x: 0, y: 1 }}
              textButton={t('CaNhanScreen.seeMedicalRecords')}
              styleTextButton={{ fontWeight: 'bold' }}
              style={{ width: 102, height: 32, marginTop: 37 }}
            />
          </View>

          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <Feather name="search" size={20} color={color.black} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.indexTracking')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
            <View style={{ borderBottomWidth: 3, borderBottomColor: color.Neutral_100, height: '100%', width: 3 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <Feather name="search" size={20} color={color.black} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.managePrescriptions')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginHorizontal: 16,
            backgroundColor: color.white,
            borderRadius: 8,
            marginTop: 26,
            marginBottom: 43,
          }}>
          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <Feather name="user" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.personalInformation')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <Image source={ICON.iconLogoTrueHope} style={{ marginHorizontal: 16, width: 16, height: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.aboutUs')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <Feather name="book-open" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.userManual')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ flexDirection: 'row', paddingVertical: 14 }}>
            <MaterialIcons name="support-agent" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.support')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
          <TouchableOpacity
            style={{ flexDirection: 'row', paddingVertical: 14 }}
            onPress={() => navigation.navigate('SettingScreen')}>
            <Feather name="settings" size={20} color={color.Neutral_500} style={{ marginHorizontal: 16 }} />
            <Text style={{ flex: 1, color: color.Neutral_900, fontSize: 14, fontWeight: 'bold' }}>
              {t('CaNhanScreen.privacySettings')}
            </Text>
            <Feather name="chevron-right" size={20} color={color.Primary_500} style={{ marginHorizontal: 16 }} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default CaNhanScreen;

const styles = StyleSheet.create({});
