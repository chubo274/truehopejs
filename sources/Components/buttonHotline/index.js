/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';
import color from '../../Helpers/color';

const ButtonHotline = ({ urlLeft, urlRight, text, onPress }) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flex: 1,
        height: 40,
        borderRadius: 24,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: color.hotline,
        marginHorizontal: 16,
      }}>
      <Image style={{ width: 20, height: 20 }} source={urlLeft} />
      <Text
        style={{
          fontSize: 15,
          fontWeight: '600',
          color: color.Semantic_Error_900,
          marginHorizontal: 7,
        }}>
        {text}
      </Text>
      <Image style={{ width: 20, height: 20 }} source={urlRight} />
    </TouchableOpacity>
  );
};

export default ButtonHotline;

const styles = StyleSheet.create({});
