/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { ICON } from '../../assets';
import color from '../../Helpers/color';
import { useTranslation } from 'react-i18next';

const CheckCustom = ({ page }) => {
  const { t } = useTranslation();
  const colorChecked = color.Primary_500;
  const colorUnCheck = color.Neutral_400;
  switch (page) {
    case 1:
      return (
        <View
          style={{
            flexDirection: 'row',
            height: 45,
            marginTop: 62,
            marginHorizontal: 75,
            paddingHorizontal: 8,
          }}>
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorChecked,
            }}
          />
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorUnCheck,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorUnCheck,
            }}
          />
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorUnCheck,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorUnCheck,
            }}
          />
          <Text style={{ position: 'absolute', bottom: 0, color: color.Primary_500 }}>{`${t('step')} 1`}</Text>
          <Text style={{ position: 'absolute', bottom: 0, left: 82 }}>{`${t('step')} 2`}</Text>
          <Text style={{ position: 'absolute', bottom: 0, right: 0 }}>{`${t('step')} 3`}</Text>
        </View>
      );
    case 2:
      return (
        <View
          style={{
            flexDirection: 'row',
            height: 45,
            marginTop: 62,
            marginHorizontal: 75,
            paddingHorizontal: 8,
          }}>
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: colorChecked,
            }}>
            <Image style={{ width: 16, height: 16 }} source={ICON.iconCheck} />
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorChecked,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorChecked,
            }}
          />
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorUnCheck,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorUnCheck,
            }}
          />
          <Text style={{ position: 'absolute', bottom: 0, color: color.Primary_500 }}>Bước 1</Text>
          <Text
            style={{
              position: 'absolute',
              bottom: 0,
              left: 82,
              color: colorChecked,
            }}>
            Bước 2
          </Text>
          <Text style={{ position: 'absolute', bottom: 0, right: 0 }}>Bước 3</Text>
        </View>
      );
    case 3:
      return (
        <View
          style={{
            flexDirection: 'row',
            height: 45,
            marginTop: 62,
            marginHorizontal: 75,
            paddingHorizontal: 8,
          }}>
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: colorChecked,
            }}>
            <Image style={{ width: 16, height: 16 }} source={ICON.iconCheck} />
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorChecked,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: colorChecked,
            }}>
            <Image style={{ width: 16, height: 16 }} source={ICON.iconCheck} />
          </View>
          <View
            style={{
              flex: 1,
              borderWidth: 1,
              height: 0,
              borderColor: colorChecked,
              marginVertical: 12,
            }}
          />
          <View
            style={{
              width: 24,
              height: 24,
              borderRadius: 12,
              borderWidth: 2,
              borderColor: colorChecked,
            }}
          />
          <Text style={{ position: 'absolute', bottom: 0, color: color.Primary_500 }}>Bước 1</Text>
          <Text
            style={{
              position: 'absolute',
              bottom: 0,
              left: 82,
              color: colorChecked,
            }}>
            Bước 2
          </Text>
          <Text
            style={{
              position: 'absolute',
              bottom: 0,
              right: 0,
              color: colorChecked,
            }}>
            Bước 3
          </Text>
        </View>
      );
    default:
      return null;
  }
};

export default CheckCustom;

const styles = StyleSheet.create({});
