/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import { View, FlatList, TouchableOpacity, TextInput, ActivityIndicator } from 'react-native';
import AppText from '../AppText';
import styles from './styles';
import { color, fontSize, languageUtils, appTimer } from '../../Helpers';
import ReactNativeModal from 'react-native-modal';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import AppFlatlistEmpty from '../AppFlatlistEmpty';
import _ from 'lodash';
import AppHeader from '../AppHeader';

const { stringToSlug } = languageUtils;

export interface DropDownProps {
  data?: Array<{ label: string, value: string }>;
  title?: string;
  titleDropDown?: string;
  value?: any;
  onChoose?: (item) => {};
  disabled?: boolean;
  require?: boolean;
  isSearch?: boolean;
  loading?: boolean;
}

export default (props: DropDownProps) => {
  const { title, onChoose, data, value: valueDefault, isSearch, loading } = props;
  const [isOpen, setOpen] = useState(false);
  const [value, setValue] = useState(valueDefault ? valueDefault : '');
  const [dataLocal, setDataLocal] = useState(data);

  const [loadingLocal, setLoading] = useState(false);

  const handleData = (itemSelected) => () => {
    setOpen(!isOpen);
    setValue(itemSelected.value);
    onChoose(itemSelected.value);
  };

  const getStyle = (index) => {
    if (index % 2 === 0) {
      return styles.selectValue0;
    } else {
      return styles.selectValue1;
    }
  };

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={handleData(item)} style={getStyle(index)}>
        <AppText style={{ color: color.black, fontSize: fontSize.f16 }}>{item.label}</AppText>
      </TouchableOpacity>
    );
  };

  const closeModal = () => {
    setOpen(!isOpen);
  };

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  useEffect(() => {
    setDataLocal(props.data);
  }, [props.data]);

  let label = '';
  const seletedValue = props.data && props.data.filter((elm) => value && elm.value.toString() === value.toString());
  if (seletedValue && seletedValue.length > 0) {
    label = seletedValue[0].label;
  }

  const deboundSearch = (txt) => {
    const newData = _.cloneDeep(props.data).filter((elm) => {
      const labelItem = stringToSlug(elm.label).toLowerCase();
      const txtInput = stringToSlug(txt).toLowerCase();
      return labelItem.includes(txtInput);
    });
    setDataLocal(newData);
    setLoading(false);
  };

  const onSearch = (txt) => {
    setLoading(true);
    appTimer.debounce(() => deboundSearch(txt), 800);
  };

  return (
    <>
      <View style={[styles.container, props.customContainer]}>
        <View style={styles.content}>
          {title && (
            <AppText style={[styles.txtTitle, { opacity: props.disabled ? 0.5 : 1 }]}>
              {title}
              {props.require && <AppText style={styles.require}> (*)</AppText>}
            </AppText>
          )}
          <TouchableOpacity
            disabled={props.disabled}
            style={[styles.multiSelectBtn, { opacity: props.disabled ? 0.5 : 1 }]}
            onPress={() => setOpen(!isOpen)}>
            <AppText numberOfLines={1} style={styles.txtValueSelected}>
              {value && label.trim()}
            </AppText>
            {loading ? (
              <ActivityIndicator size="small" />
            ) : (
              <MIcon name="chevron-down" style={styles.iconDown} resizeMode="contain" />
            )}
          </TouchableOpacity>
        </View>
      </View>

      <ReactNativeModal
        style={styles.containerModal}
        useNativeDriver
        isVisible={isOpen}
        onBackdropPress={closeModal}
        animationIn="slideInUp"
        animationOut="slideOutDown">
        <AppHeader iconLeft="chevron-left" onLeftPress={closeModal} title={props.titleDropDown} />
        {isSearch && <TextInput style={styles.searchBox} placeholder="Tìm kiếm" onChangeText={onSearch} />}
        <FlatList
          data={dataLocal}
          refreshing={loadingLocal}
          onRefresh={() => {}}
          renderToHardwareTextureAndroid
          decelerationRate={0.95}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
          keyExtractor={(item) => item && item.value.toString()}
          ListEmptyComponent={() => <AppFlatlistEmpty />}
        />
      </ReactNativeModal>
    </>
  );
};
