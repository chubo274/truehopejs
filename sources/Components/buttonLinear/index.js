/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import color from '../../Helpers/color';

const ButtonLinear = ({
  urlLeft,
  urlRight,
  textButton,
  colors,
  angle,
  angleCenter,
  onPress,
  style,
  styleTextButton,
}) => {
  return (
    <LinearGradient
      style={[styles.linear, style]}
      colors={colors}
      useAngle={true}
      angle={angle}
      angleCenter={angleCenter}>
      <TouchableOpacity style={styles.touchableOpacity} onPress={onPress}>
        <Image style={styles.iconImg} source={urlLeft} />
        <Text style={[styles.textButton, styleTextButton]}>{textButton}</Text>
        <Image style={styles.iconImg} source={urlRight} />
      </TouchableOpacity>
    </LinearGradient>
  );
};
export default React.memo(ButtonLinear);
const styles = StyleSheet.create({
  linear: {
    flex: 1,
    height: 48,
    borderRadius: 24,
    marginHorizontal: 16,
  },
  touchableOpacity: {
    flex: 1,
    height: 48,
    borderRadius: 24,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    fontSize: 16,
    fontWeight: '600',
    color: color.Neutral_100,
    marginHorizontal: 7,
  },
  iconImg: { width: 18, height: 18 },
});
