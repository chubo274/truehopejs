import { StyleSheet } from 'react-native';
import { color, fontSize } from '../../Helpers';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
});
