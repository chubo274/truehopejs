import React from 'react';
import { ViewProps, View } from 'react-native';
import styles from './styles';
import AppHeader, { HeaderProps } from '../AppHeader';

interface Props extends HeaderProps {}

export default (props: Props) => {
  return (
    <View style={styles.container}>
      <AppHeader {...props} />
      <View style={styles.content}>{props.children}</View>
    </View>
  );
};
