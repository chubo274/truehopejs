import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { responsivePixel } from '../../Helpers';
import color from '../../helpers/color';

interface AppFlatlistFooterProps {
  isVisible: boolean;
}
export default (props: AppFlatlistFooterProps) => {
  if (!props.isVisible) {
    return null;
  }
  return (
    <View style={styles.container}>
      <ActivityIndicator color={color.orange} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: responsivePixel(36),
    // flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
