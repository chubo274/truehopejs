import React from 'react';
import { Text, TextStyle, TouchableOpacityProps } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';

interface AppButtonProps extends TouchableOpacityProps {
  children?: any;
  title?: any;
  textStyle?: TextStyle;
  transparent?: boolean;
}
const AppButton = (props: AppButtonProps) => {
  return (
    <TouchableOpacity
      {...props}
      onPress={props.onPress}
      style={[!props.transparent && styles.defaultStyle, props.style]}>
      {props.title ? <Text style={[styles.defaultTextStyle, props.textStyle]}>{props.title}</Text> : props.children}
    </TouchableOpacity>
  );
};
export default AppButton;
