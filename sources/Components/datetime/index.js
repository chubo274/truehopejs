/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import moment from 'moment';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import color from '../../Helpers/color';

const DateTime = ({datetime, changeDatetime, disabled}) => {
  const [isShowDatePicker, setIsShowDatePicker] = useState(false);

  const toggleShowDatePicker = () => {
    setIsShowDatePicker(!isShowDatePicker);
  };
  return (
    <View style={[styles.ViewTextInput]}>
      <Icon name="birthday-cake" size={15} style={styles.icon} />
      <DateTimePickerModal
        isVisible={isShowDatePicker}
        onCancel={toggleShowDatePicker}
        onConfirm={date => {
          toggleShowDatePicker();
          changeDatetime(moment(date).toDate());
        }}
        date={moment(datetime).toDate()}
      />
      <Text style={{flex: 1}}>{moment(datetime).format('DD-MM-YYYY')}</Text>
      <TouchableOpacity onPress={toggleShowDatePicker} disabled={disabled}>
        <Feather name={'calendar'} size={15} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

export default DateTime;
const styles = StyleSheet.create({
  ViewTextInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 16,
    backgroundColor: color.white,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: color.Neutral_300,
    marginTop: 12,
    height: 44,
  },
  icon: {width: 16, height: 16, margin: 14, color: color.Neutral_500},
});
