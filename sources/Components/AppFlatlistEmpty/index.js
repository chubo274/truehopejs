import React from 'react';
import { View, StyleSheet } from 'react-native';
import { device } from '../../Helpers';
import AppText from '../AppText';

interface AppFlatlistFooterProps {
  isVisible: boolean;
  message: string;
}
export default (props: AppFlatlistFooterProps) => {
  const { message } = props;
  return (
    <View style={styles.container}>
      <AppText>{message || 'Danh sách rỗng'}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: device.height * 0.2,
    width: device.width,
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
