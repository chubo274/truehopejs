import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import color from '../../Helpers/color';

const ButtonCustom = ({ urlLeft, urlRight, textButton, style, onPress, styleText }) => {
  return (
    <TouchableOpacity style={[styles.touchableOpacity, style]} onPress={onPress}>
      <Image style={styles.iconImg} resizeMode={'contain'} source={urlLeft} />
      <Text style={[styles.textButton, styleText]}>{textButton}</Text>
      <Image style={styles.iconImg} resizeMode={'contain'} source={urlRight} />
    </TouchableOpacity>
  );
};

export default React.memo(ButtonCustom);

const styles = StyleSheet.create({
  touchableOpacity: {
    flex: 1,
    height: 48,
    borderRadius: 24,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 16,
  },
  textButton: {
    fontSize: 16,
    fontWeight: '600',
    color: color.Neutral_100,
    marginHorizontal: 7,
  },
  iconImg: { width: 20, height: 20 },
});
