import React, { useState } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import color from '../../Helpers/color';

const TextInputCustom = ({
  placeholder,
  nameIconLeft,
  nameIconRight,
  value,
  onChangeText,
  onPressRight,
  secureTextEntry,
  keyboardType,
}) => {
  const [isfocus, setIsfocus] = useState(false);
  const onchangeIsfocus = (focus) => {
    setIsfocus(focus);
  };

  return (
    <View
      style={[
        styles.ViewTextInput,
        {
          borderColor: isfocus ? color.Secondary_500 : color.Neutral_300,
        },
      ]}>
      <Feather
        name={nameIconLeft}
        color={isfocus ? color.Secondary_500 : color.Neutral_500}
        size={15}
        style={styles.icon}
      />
      <TextInput
        placeholder={placeholder}
        style={styles.TextInput}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        onFocus={() => {
          onchangeIsfocus(true);
        }}
        onBlur={() => {
          onchangeIsfocus(false);
        }}
        keyboardType={keyboardType}
      />
      <TouchableOpacity onPress={onPressRight}>
        <Feather
          name={nameIconRight}
          color={isfocus ? color.Secondary_500 : color.Neutral_500}
          size={15}
          style={styles.icon}
        />
      </TouchableOpacity>
    </View>
  );
};

export default TextInputCustom;

const styles = StyleSheet.create({
  ViewTextInput: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 16,
    backgroundColor: color.white,
    borderRadius: 4,
    borderWidth: 1,
    marginTop: 12,
  },
  TextInput: {
    flex: 1,
    height: 44,
  },
  icon: { width: 16, height: 16, margin: 14 },
});
