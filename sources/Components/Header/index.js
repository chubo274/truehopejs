/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import color from '../../Helpers/color';
import Feather from 'react-native-vector-icons/Feather';

const Header = ({
  iconLeft,
  iconRight1,
  iconRight2,
  textHeader,
  onPressIconLeft,
  onPressIconRight1,
  onPressIconRight2,
}) => {
  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: color.new_bg_900,
          height: 56,
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity style={{ height: '100%', justifyContent: 'center' }} onPress={onPressIconLeft}>
          <Feather name={iconLeft} size={20} color={color.white} style={{ marginHorizontal: 16 }} />
        </TouchableOpacity>
        <Text style={{ color: color.white, fontSize: 18, fontWeight: 'bold' }}>{textHeader}</Text>
        <View style={{ flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', height: '100%' }}>
          <TouchableOpacity style={{ justifyContent: 'center', height: '100%' }} onPress={onPressIconRight1}>
            <Feather name={iconRight1} size={20} color={color.white} style={{ marginHorizontal: 6 }} />
          </TouchableOpacity>
          <TouchableOpacity style={{ justifyContent: 'center', height: '100%' }} onPress={onPressIconRight2}>
            <Feather name={iconRight2} size={20} color={color.white} style={{ marginHorizontal: 6 }} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({});
