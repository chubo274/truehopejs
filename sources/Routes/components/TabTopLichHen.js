/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import color from '../../Helpers/color';
import Header from '../../Components/Header';
import { useTranslation } from 'react-i18next';

const TabTopLichHen = (props) => {
  const { t } = useTranslation();
  console.log('props', props);
  const listLabel = [t('LichHenScreen.upcoming'), t('LichHenScreen.arrived')];
  return (
    <View>
      <Header
        textHeader={t('LichHenScreen.yourAppointment')}
        iconLeft={'chevron-left'}
        iconRight1={'search'}
        iconRight2={'filter'}
      />
      <View style={{ flexDirection: 'row', marginHorizontal: 14 }}>
        {props.state.routes.map((route, index) => {
          const label = listLabel[index];
          const isFocused = props.state.index === index;
          const onPress = () => {
            props.navigation.navigate(route.name);
          };
          return (
            <TouchableOpacity key={route.key} onPress={onPress} style={{ flex: 1, marginHorizontal: 1 }}>
              {/* style={{ flex: 1, borderBottomWidth: isFocused ? 4 : 0, borderColor: color.Accent_900 }}> */}
              <Text style={[styles.textLabel, isFocused ? { color: color.Accent_900 } : { color: color.Neutral_400 }]}>
                {label}
              </Text>
              <View
                style={[
                  { flex: 1, borderWidth: 2, borderRadius: 2 },
                  isFocused ? { borderColor: color.Accent_900 } : { borderColor: color.Neutral_300 },
                ]}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    </View>
  );
};

export default TabTopLichHen;

const styles = StyleSheet.create({
  textLabel: {
    textAlign: 'center',
    lineHeight: 60,
    fontSize: 15,
    fontWeight: 'bold',
  },
});
