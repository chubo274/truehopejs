/* eslint-disable react-native/no-inline-styles */
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import _ from 'lodash';
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import HomePage from '../Modules/HomePageScreen';
import QRCodeScreen from '../Modules/HomePageScreen/QRCodeScreen';
import LoginScreen from '../Modules/loginScreen';
import RegisterInfoScreen from '../Modules/registerFolder/registerInfoScreen';
import RegisterOtpScreen from '../Modules/registerFolder/registerOtpScreen';
import RegisterScreen from '../Modules/registerFolder/registerScreen';
import RegisterSuccessScreen from '../Modules/registerFolder/registerSuccessScreen';
import SearchScreen from '../Modules/SearchScreen';
import SettingScreen from '../Modules/SettingScreen';
import test from '../Modules/test';
import { setToken } from '../Services/serviceHandle';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import color from '../Helpers/color';
import LichHenSapDienRaScreen from '../Modules/LichHenFolder/LichHenSapDienRa';
import LichHenDaDienRaScreen from '../Modules/LichHenFolder/LichHenDaDienRa';
import ThongBaoScreen from '../Modules/ThongBaoScreen';
import TabTopLichHen from './components/TabTopLichHen';
import CaNhanScreen from '../Modules/CaNhanScreen';

const RootStack = createStackNavigator();
const TokenStack = createStackNavigator();
const NoTokenStack = createStackNavigator();
const RegisterStack = createStackNavigator();
const Tab = createBottomTabNavigator();
const TabLichHen = createMaterialTopTabNavigator();
const LichHen = () => {
  return (
    <TabLichHen.Navigator tabBar={(props) => <TabTopLichHen {...props} swipeEnabled={false} />}>
      <TabLichHen.Screen name="LichHenSapDienRaScreen" component={LichHenSapDienRaScreen} />
      <TabLichHen.Screen name="LichHenDaDienRaScreen" component={LichHenDaDienRaScreen} />
    </TabLichHen.Navigator>
  );
};
const TabNavigation = () => {
  return (
    <Tab.Navigator tabBarOptions={{ showLabel: false }}>
      <Tab.Screen
        name="HomePage"
        component={HomePage}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.viewTabBar}>
              {focused && <View style={[styles.viewBorder, { top: 0 }]} />}
              <FontAwesome name="home" size={20} style={{ color: focused ? color.Accent_700 : color.Neutral_400 }} />
              <Text style={{ color: focused ? color.Accent_900 : color.Neutral_500, fontSize: 12 }}>Trang chủ</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.viewTabBar}>
              {focused && <View style={[styles.viewBorder, { top: 0 }]} />}
              <FontAwesome name="search" size={20} style={{ color: focused ? color.Accent_700 : color.Neutral_400 }} />
              <Text style={{ color: focused ? color.Accent_900 : color.Neutral_500, fontSize: 12 }}>Tìm kiếm</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="LichHen"
        component={LichHen}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.viewTabBar}>
              {focused && <View style={[styles.viewBorder, { top: 0 }]} />}
              <FontAwesome
                name="calendar-o"
                size={20}
                style={{ color: focused ? color.Accent_700 : color.Neutral_400 }}
              />
              <Text style={{ color: focused ? color.Accent_900 : color.Neutral_500, fontSize: 12 }}>Lịch hẹn</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="ThongBaoScreen"
        component={ThongBaoScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.viewTabBar}>
              {focused && <View style={[styles.viewBorder, { top: 0 }]} />}
              <FontAwesome name="bell" size={20} style={{ color: focused ? color.Accent_700 : color.Neutral_400 }} />
              <Text style={{ color: focused ? color.Accent_900 : color.Neutral_500, fontSize: 12 }}>Thông báo</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="CaNhanScreen"
        component={CaNhanScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <View style={styles.viewTabBar}>
              {focused && <View style={[styles.viewBorder, { top: 0 }]} />}
              <FontAwesome name="user" size={20} style={{ color: focused ? color.Accent_700 : color.Neutral_400 }} />
              <Text style={{ color: focused ? color.Accent_900 : color.Neutral_500, fontSize: 12 }}>Cá nhân</Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const Token = () => {
  return (
    <TokenStack.Navigator initialRouteName={''} headerMode="none">
      <NoTokenStack.Screen name="TabNavigation" component={TabNavigation} />
      <NoTokenStack.Screen name="HomePage" component={HomePage} />
      <NoTokenStack.Screen name="QRCodeScreen" component={QRCodeScreen} />
      <NoTokenStack.Screen name="SettingScreen" component={SettingScreen} />
      <TokenStack.Screen name="test" component={test} />
    </TokenStack.Navigator>
  );
};

const StackRegister = () => {
  return (
    <RegisterStack.Navigator headerMode="none">
      <RegisterStack.Screen name="RegisterScreen" component={RegisterScreen} />
      <RegisterStack.Screen name="RegisterOtpScreen" component={RegisterOtpScreen} />
      <RegisterStack.Screen name="RegisterInfoScreen" component={RegisterInfoScreen} />
      <RegisterStack.Screen name="RegisterSuccessScreen" component={RegisterSuccessScreen} />
    </RegisterStack.Navigator>
  );
};

const NoToken = () => {
  return (
    <NoTokenStack.Navigator headerMode="none">
      {/* <NoTokenStack.Screen name="TabNavigation" component={TabNavigation} />
      <NoTokenStack.Screen name="HomePage" component={HomePage} />
      <NoTokenStack.Screen name="QRCodeScreen" component={QRCodeScreen} /> */}
      <NoTokenStack.Screen name="LoginScreen" component={LoginScreen} />
      <NoTokenStack.Screen name="StackRegister" component={StackRegister} />
    </NoTokenStack.Navigator>
  );
};

const RootStackScreen = () => {
  const userReducer = useSelector((state) => state.userReducer);
  const userToken = !_.isEmpty(userReducer.LoginReducer?.data?.access_token);
  const nameUser = !_.isEmpty(userReducer.LoginReducer?.data?.getDataUser?.data?.name);
  const last_nameUser = !_.isEmpty(userReducer.LoginReducer?.data?.user?.last_name);
  const first_nameUser = !_.isEmpty(userReducer.LoginReducer?.data?.user?.first_name);

  if (userToken) {
    setToken(userReducer.LoginReducer?.data?.access_token);
  }

  // const userToken = _.isEmpty(userReducer.data);
  console.log('123', userReducer);
  console.log({ userToken });
  return (
    <RootStack.Navigator headerMode="none">
      {/* {(userToken, nameUser, last_nameUser, first_nameUser) ? ( */}
      {userToken ? (
        <RootStack.Screen name="Token" component={Token} options={{ animationEnabled: true }} />
      ) : (
        <RootStack.Screen name="NoToken" component={NoToken} options={{ animationEnabled: true }} />
      )}
    </RootStack.Navigator>
  );
};

export default RootStackScreen;

const styles = StyleSheet.create({
  viewTabBar: { alignItems: 'center', justifyContent: 'center', flex: 1 },
  viewBorder: { borderWidth: 2, width: '100%', position: 'absolute', borderColor: color.Accent_900 },
});
