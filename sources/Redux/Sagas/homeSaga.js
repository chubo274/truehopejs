import { call, put, takeLatest } from 'redux-saga/effects';
import { url } from '../../Services/serviceBase';
import { get } from '../../Services/serviceHandle';
import ActionTypes from '../ActionTypes';
import { isEmpty } from 'lodash';

export function* getNews(payload) {
  console.log({ payload });
  const response = yield call(get, `${url.news}?limit=${payload.limit}&offset=${payload.offset}`);
  if (!isEmpty(response.results)) {
    yield put({ type: ActionTypes.GET_NEW_SUCCESS, data: response });
  } else {
    yield put({ type: ActionTypes.GET_NEW_FAILED });
  }
}
export function* getBookings(payload) {
  const onSuccess = payload?.callbacks?.onSuccess;
  const onFailed = payload?.callbacks?.onFailed;
  const response = yield call(get, url.bookings);
  if (response.code === 200) {
    onSuccess && onSuccess(response.data);
  } else {
    onFailed && onFailed(response);
  }
}
export function* getOffers(payload) {
  const onSuccess = payload?.callbacks?.onSuccess;
  const onFailed = payload?.callbacks?.onFailed;
  const response = yield call(get, url.offers);
  if (!isEmpty(response.results)) {
    onSuccess && onSuccess(response.results);
  } else {
    onFailed && onFailed(response);
  }
}
export default function* () {
  yield takeLatest(ActionTypes.GET_NEW_REQUEST, getNews);
  yield takeLatest(ActionTypes.GET_BOOKINGS_REQUEST, getBookings);
  yield takeLatest(ActionTypes.GET_OFFERS_REQUEST, getOffers);
}
