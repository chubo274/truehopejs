import { Alert } from 'react-native';
import { call, put, takeLatest } from 'redux-saga/effects';
import ActionTypes from '../../Redux/ActionTypes';
import { url } from '../../Services/serviceBase';
import { get, patch, post, setToken } from '../../Services/serviceHandle';

export function* login(payload) {
  const response = yield call(post, url.login, payload.data);
  setToken(response.data?.access_token);
  if (response.code === 200) {
    yield put({ type: ActionTypes.LOGIN_SUCCESS, data: response.data });
  } else {
    console.log('false');
    yield put({ type: ActionTypes.LOGIN_FAILED, error: response.message });
  }
}
export function* register(payload) {
  const response = yield call(post, url.register, payload.data);
  if (response.code === 200) {
    yield put({ type: ActionTypes.REGISTER_SUCCESS, data: response.data });
  } else {
    yield put({
      type: ActionTypes.REGISTER_FAILED,
      error: response.message,
    });
    Alert.alert(response.message);
  }
}

export function* updateUserInfo(payload) {
  const response = yield call(patch, url.userInfo, payload.data);
  if (response.code === 200) {
    yield put({ type: ActionTypes.UPDATE_USER_INFO_SUCCESS, data: response.data });
  } else {
    yield put({ type: ActionTypes.UPDATE_USER_INFO_FAILED });
  }
}

export function* checkPhone(payload) {
  const response = yield call(post, url.checkPhoneRegister, payload.data);
  if (response.code === 404) {
    yield put({ type: ActionTypes.CHECK_PHONE_SUCCESS });
  } else {
    yield put({ type: ActionTypes.CHECK_PHONE_FAILED });
  }
}
export function* QRCode(payload) {
  const onSuccess = payload?.callbacks?.onSuccess;
  const onFailed = payload?.callbacks?.onFailed;
  const response = yield call(get, url.QRCode);
  console.log({ response });
  // onSuccess && onSuccess(response);
}
export default function* () {
  yield takeLatest(ActionTypes.LOGIN_REQUEST, login);
  yield takeLatest(ActionTypes.REGISTER_REQUEST, register);
  yield takeLatest(ActionTypes.UPDATE_USER_INFO_REQUEST, updateUserInfo);
  yield takeLatest(ActionTypes.CHECK_PHONE_REQUEST, checkPhone);
  yield takeLatest(ActionTypes.QR_CODE, QRCode);
}
