import { all, fork } from 'redux-saga/effects';
import homeSaga from './homeSaga';
import userSaga from './userSaga';

export default function* watch() {
  yield all([fork(userSaga), fork(homeSaga)]);
}
