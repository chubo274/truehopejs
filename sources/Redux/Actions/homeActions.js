import ActionTypes from '../ActionTypes';

export const getNews = ({ limit = 10, offset = 0 }) => {
  return {
    type: ActionTypes.GET_NEW_REQUEST,
    limit,
    offset,
  };
};
export const getBookings = (callbacks) => {
  return {
    type: ActionTypes.GET_BOOKINGS_REQUEST,
    callbacks,
  };
};
export const getOffers = (callbacks) => {
  return {
    type: ActionTypes.GET_OFFERS_REQUEST,
    callbacks,
  };
};
