import ActionTypes from '../ActionTypes';

/*
 * Reducer actions related with login
 */

export const login = (data) => {
  return {
    type: ActionTypes.LOGIN_REQUEST,
    data,
  };
};
export const logout = () => {
  return {
    type: ActionTypes.LOG_OUT,
  };
};
export const register = (data) => {
  return {
    type: ActionTypes.REGISTER_REQUEST,
    data,
  };
};
export const updateUserInfo = (data) => {
  return {
    type: ActionTypes.UPDATE_USER_INFO_REQUEST,
    data,
  };
};
export const checkPhoneRegister = (data) => {
  return {
    type: ActionTypes.CHECK_PHONE_REQUEST,
    data,
  };
};
export const QRCode = (callbacks) => {
  return {
    type: ActionTypes.QR_CODE,
    callbacks,
  };
};
