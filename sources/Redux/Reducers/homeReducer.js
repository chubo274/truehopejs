import { combineReducers } from 'redux';
import ActionTypes from '../ActionTypes';

const initialState = {
  type: '',
  data: {},
  error: {},
};
export const tinTucReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.GET_NEW_SUCCESS:
      return {
        ...state,
        type: ActionTypes.GET_NEW_SUCCESS,
        data: action.data,
      };
    case ActionTypes.GET_NEW_FAILED: {
      return {
        ...state,
        type: ActionTypes.GET_NEW_FAILED,
      };
    }
    default:
      return state;
  }
};
export default combineReducers({ tinTucReducer });
