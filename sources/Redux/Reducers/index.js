import { combineReducers } from 'redux';
// Redux: Root Reducer
import userReducer from './userReducer';
import homeReducer from './homeReducer';

const rootReducer = combineReducers({
  userReducer,
  homeReducer,
});
// Exports
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
