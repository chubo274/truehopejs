import { combineReducers } from 'redux';
import ActionTypes from '../ActionTypes';

const initialState = {
  type: '',
  data: {},
  error: {},
};
export const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        type: ActionTypes.LOGIN_SUCCESS,
        data: action.data,
      };
    case ActionTypes.LOGIN_FAILED:
      return {
        ...state,
        type: ActionTypes.LOGIN_FAILED,
        error: action.error,
      };
    case ActionTypes.LOG_OUT:
      return initialState;
    case '':
      return {
        ...state,
        type: '',
        error: {},
      };
    default:
      return state;
  }
};
export const RegisterReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        type: ActionTypes.REGISTER_SUCCESS,
        data: action.data,
      };
    case ActionTypes.REGISTER_FAILED:
      return {
        ...state,
        type: ActionTypes.REGISTER_FAILED,
        error: action.error,
      };
    case '':
      return {
        ...state,
        type: '',
        error: {},
      };
    default:
      return state;
  }
};
export const CheckPhoneReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.CHECK_PHONE_SUCCESS:
      return {
        ...state,
        type: ActionTypes.CHECK_PHONE_SUCCESS,
      };
    case ActionTypes.CHECK_PHONE_FAILED:
      return {
        ...state,
        type: ActionTypes.CHECK_PHONE_FAILED,
      };
    case '':
      return {
        ...state,
        type: '',
        error: {},
      };
    default:
      return state;
  }
};
export const UpdateUserInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_USER_INFO_SUCCESS:
      return {
        ...state,
        type: ActionTypes.UPDATE_USER_INFO_SUCCESS,
      };
    case ActionTypes.UPDATE_USER_INFO_FAILED:
      return {
        ...state,
        type: ActionTypes.UPDATE_USER_INFO_FAILED,
      };
    case '':
      return {
        ...state,
        type: '',
        error: {},
      };
    default:
      return state;
  }
};
export const QRCodeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.QR_CODE:
      return {
        ...state,
        type: ActionTypes.QR_CODE,
        data: action.data,
      };

    default:
      return state;
  }
};

export default combineReducers({
  LoginReducer,
  RegisterReducer,
  CheckPhoneReducer,
  UpdateUserInfoReducer,
  QRCodeReducer,
});
