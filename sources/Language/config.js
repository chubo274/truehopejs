import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

i18n.use(initReactI18next).init({
  fallbackLng: 'vn',
  lng: 'vn',
  resources: {
    vn: {
      translations: require('./vn'),
    },
    en: {
      translations: require('./en'),
    },
  },
  ns: ['translations'],
  defaultNS: 'translations',
});

i18n.languages = ['vn', 'en'];

export default i18n;
