import axios from 'axios';
import { baseUrl } from './serviceBase';

const api = axios.create({
  baseURL: baseUrl,
  timeout: 30000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

const setToken = (token) => {
  api.defaults.headers.common.Authorization = `Bearer ${token}`;
};

const post = async (url, data) => {
  try {
    const response = await api.post(url, data);
    return handleResponse(response);
  } catch (error) {
    return handleResponse(error.response);
  }
};
const get = async (url, data) => {
  try {
    const response = await api.get(url, data);
    return handleResponse(response);
  } catch (error) {
    return handleResponse(error.response);
  }
};
const patch = async (url, data) => {
  try {
    const response = await api.patch(url, data);
    return handleResponse(response);
  } catch (error) {
    return handleResponse(error.response);
  }
};

const handleResponse = (res) => {
  // console.log(res);
  return res.data;
};

export { get, post, patch, setToken };
