export default {
  url: {
    // HOST: 'http://10.0.142.114:8080/',
    // staging
    HOST: 'https://ssm-uat-api.vnpost.vn',
    login: 'api/TokenAuth/Authenticate',
    sessionInfo: '/api/services/app/Session/GetCurrentLoginInformations',
    // get danh muc
    dmDonVi: '/api/services/app/DanhMucDonVi/GetForDropDownListKeKhai',
    dmPhuongAnTN: '/api/services/app/DanhMucPhuongAnBHXHTN/GetAllNoPaging',
    dmTinh: '/api/services/app/DanhMucTinh/GetForDropDownList',
    dmHuyen: '/api/services/app/DanhMucHuyen/GetForDropDownList',
    dmXa: '/api/services/app/DanhMucXa/GetForDropDownList',
    dmPhuongAnBHYT: '/api/services/app/DanhMucPhuongAnBHYT/GetAllNoPaging',
    dmBenhVien: '/api/services/app/DanhMucCoSoKhamChuaBenh/GetForDropDownList',
    // end get danh muc
    nhapHoSo: '/api/services/app/KeKhai/NhapHoSo',
    getLuongCoSoHienTai: 'api/services/app/DanhMucLuongCoSo/GetLuongCoSoHienTai',
    searchThongTinBHXHTN: '/api/services/app/TraCuu/TraCuuThongTinBHXHTN',
    searchThongTinBHYT: '/api/services/app/TraCuu/TraCuuThongTinBHYT',
    getSoLieuHienTai: '/api/services/app/KeKhai/GetSoLieuHienTai',
    traCuuNoGroup: '/api/services/app/KeKhai/TraCuuNoGroup',
    danhSachSuVu: '/api/services/app/SuVu/LayDanhSachSuVu',
    //
    xoaHoSo: 'api/services/app/KeKhai/XoaToKhai?transactionId=',
    nopHoSo: '/api/services/app/KeKhai/NopHoSo?currentMangLuoiId=',
    getHoSo: '/api/services/app/KeKhai/GetHoSo',
    editHoso: '/api/services/app/KeKhai/CapNhatHoSo',
    traCuuBHXH: '/api/services/app/TraCuu/TraCuuMaSoBHXH',
    traCuuHGD: '/api/services/app/TraCuu/TraCuuThongTinHGD',
    taoSuVuHuy: 'api/services/app/SuVu/TaoSuVuHuy',

    // post báo cáo thống kế
    searchThongKeHoSo: '/api/services/app/BaoCaoTongHopGDThu/BaoCaoTongHopTheoDonVi',
    searchThuLaoDaiLy: '/api/services/app/BaoCaoTongHopGDThu/BaoCaoChiThuLaoTheoNhanVien',

    // in C68
    inC68: '/api/services/app/KeKhai/InBienLaiC68Hd',
  },
  statusCode: {
    success: [200, 201, 204],
    auth: [401],
    notFound: [404],
    error: [500, 400],
  },
};
