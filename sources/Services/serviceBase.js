export const baseUrl = 'https://zuhan.ga/api';
export const url = {
  login: '/token/',
  register: '/register/',
  userInfo: '/user/info/',
  checkPhoneRegister: '/verify/phone',
  news: '/news/',
  QRCode: '/user/qrcode/',
  bookings: '/bookings/',
  offers: '/offers/',
};
