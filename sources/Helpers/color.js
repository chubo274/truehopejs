export default {
  Neutral_100: '#F3F3F1',
  Neutral_200: '#EAEAE9',
  Neutral_300: '#DADAD9',
  Neutral_400: '#B7B7B5',
  Neutral_500: '#979796',
  Neutral_600: '#6F6F6D',
  Neutral_700: '#5B5B5A',
  Neutral_800: '#3C3C3B',
  Neutral_900: '#1C1C1B',

  Primary_100: '#B0DFDD',
  Primary_200: '#7BCBC7',
  Primary_300: '#42B6AF',
  Primary_400: '#00A69E',
  Primary_500: '#00968C',
  Primary_600: '#00897E',
  Primary_700: '#00796E',
  Primary_800: '#00695E',
  Primary_900: '#00594A',

  Secondary_500: '#249CF3',

  Accent_500: '#249CF3',
  Accent_700: '#167CD3',
  Accent_800: '#106BC1',
  Accent_900: '#034DA2',

  Gradient_2_1: '#106BC1',
  Gradient_2_2: '#034DA2',

  new_bg_900: '#241F59',

  Semantic_Error_900: '#C30003',

  gradient_button_1: '#FFE446',
  gradient_button_2: '#00A69E',

  white: '#ffffff',
  black: '#000000',
  Shadow: '#C2C6C9',
  hotline: '#E0001A',
};
