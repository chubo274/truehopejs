export default class Timer {
  constructor() {
    this.timer = null;
  }

  debounce(func, timeOut) {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(func, timeOut);
  }
}
