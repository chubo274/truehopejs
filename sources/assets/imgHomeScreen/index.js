export const img = {
  iconHome: require('./IconHome.png'),
  point: require('./Point.png'),
  QRCode: require('./QRCode.png'),
  ImgDatKham: require('./DatKham.png'),
  ImgTVTT: require('./Tu_van_truc_tuyen.png'),
  ImgNhaThuoc: require('./Img_nha_thuoc.png'),
  ImgTinTucSK: require('./tin_tuc_suc_khoe.png'),
  ImgTheoDoiCHiSo: require('./theo_doi_chi_so.png'),
  ImgYBaDienTu: require('./y_ba_dien_tu.png'),
  ImgNhacNhoSucKhoe: require('./nhac_nho_suc_khoe.png'),
  ImgKhanCap: require('./khan_cap.png'),
  slide: require('./slide.png'),
};
